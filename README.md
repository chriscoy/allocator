# CLink Allocator

Written for Node or babel-node. Run npm install.

### Version
0.0.1

### Tech

* [Node] - the chosen Javascript runtime
* [Mocha+Chai] - unit testing

### Running tests

```sh
$ npm test
```

### Expected Input
A JSON array, with each element having a minimum of two properties: role and name, as follows:
```sh
[
    { 
		"role": "Manager",
		"name": "Joe"
	}
]
```

Valid roles are:
* Department
* Manager
* QA Tester
* Developer

In the event that data is nested, it is expected under an extra property, called 'reports.' In such a case, the data might look more like this:
```sh
[ 
	{
		"role": "Department",
		"name": "Dept. A",
		"reports": [
			{ 
				"role": "Manager",
				"name": "Bob",
				"reports": [
					{ "role": "QA Tester", "name": "Janine" },
					{ "role": "Developer", "name": "Stephanie"}
				]
			}
		]
	},
	{ 
		"role": "Manager",
		"name": "Joe",
		"reports": [
			{ "role": "QA Tester", "name": "Bill" },
			{ "role": "Developer", "name": "Bertha"}
		]
	}
]
```

### Usage

```sh
var allocator = require("../app/allocations");
var fs		  = require('fs'); // IF you want to load JSON via the filesystem

var departmentHierarchy = JSON.parse( fs.readFileSync('data.json', 'utf-8') );
var budget = allocator.calculateBudget(departmentHierarchy);
```
'budget' now has two properties: total and budgetData
* total is a number, a total of all the allocations
* budgetData is an enriched version of the original data, with allocations and totals added per level. 

The enriched data would look something like this (truncated for brevity...)
```sh
{
	"total": 5400,
	"budgetData": [{
		"role": "Department",
		"name": "Dept. A",
		"reports": [{
			"role": "Manager",
			"name": "Bob",
			"reports": [{
				"role": "QA Tester",
				"name": "Janine",
				"reports": [],
				"allocation": 500,
				"totalAllocation": 500
			}, {
				"role": "Developer",
				"name": "Stephanie",
				"reports": [],
				"allocation": 1000,
				"totalAllocation": 1000
			}],
			"allocation": 300,
			"totalAllocation": 1800
		}],
		"allocation": 0,
		"totalAllocation": 1800
	}, 
```

You could look up a department or person by name and get back the total budget they represent, as follows:
```sh
var bobsMoney = allocator.lookupTotalForName("Bob", budget.budgetData); // returns a number
```

### Optional Configuration Settings
The allocation configuration is a set of role names with a corresponding budget amount. Roles can be added (or removed,) in order to alter the output of the Allocator. The allocator object exposes this configuration as a property called 'allocationConfig'
```sh
allocationConfig = [
	{ "role": "Department", "allocation": 0 },
	{ "role": "Manager", "allocation": 300 },
	{ "role": "QA Tester", "allocation": 500 },
	{ "role": "Developer", "allocation": 1000}
]
```